<table>
<tr><td>
  <p><strong>I</strong>n contrast to exchange transactions with real supply or real currency the participants of FOREX use trading with a margin deposit; i.e. marginal or leverage trading. In marginal trading, each transaction has two obligatory stages (they can be divided by period of time, which can be as long as you like): buying (selling) of currency at one price, and then selling (buying) it at another (or at the same) price. The first transaction is called <strong>opening the position</strong>, the second one, <strong>closing the position</strong>.</p>
  <p><strong>O</strong>pening a position, a trader furnishes a deposit sum from 0.5 to 4 per cent of the credit line, granted for the transaction. So, in order to buy or sell 100,000 US dollars for Japanese yens, you will not need the whole sum, but only from 500 to 2000 US dollars depending on your policy of controlling risks. When the position is closed, the deposit sum returns, and calculation of profits or losses is done. All the profit or losses caused by the change of currency rates is credited on your account.</p>
  <p><strong>L</strong>et's take a concrete example of getting a profit from the changing the rate of the Euro, from 0,9162 to 0,9292. If you have anticipated this change by using technical or fundamental analysis, you can buy the Euro cheaper for dollars, and then sell it back at a higher price. For example, if you choose <strong>leverage</strong> 1:100, then 99,000 dollars of the <strong>credit line</strong>, granted by the <strong>Internet broker</strong>, is added to 1000 dollars, and you buy the Euro at the price of 0.9162. As a result of this transaction we get: $ 100,000 / 0.9162 = Euro 109.146, 47.</p>
  <p><strong>W</strong>hen the rate changes (an average daily change of Euro is about 70 to 100 pips), you close the position and sell the Euro for dollars, but at the rate of 0.9292. You get 109,146. 47*0.9292 =101,418.89 dollars. Your profit is $ 1,418.89. The same transaction with leverage 1:200 would give you $2, 837.78 of profit, with leverage 1:50 the profit would be 709.45, with leverage 1:25 - 354.72.</p>
  <p><strong>W</strong>e'd like to remind you that the higher the credit leverage, the higher is your profit if the fluctuation of the currency rate was anticipated correctly. However, if your anticipation was wrong, your losses will be bigger.</p>
  <p><strong>O</strong>ne cannot feel confident in the FOREX market without a thorough knowledge of the terms used there.</p>
  <p><em><strong>Foreign exchange quotes</strong></em> are a relation between currencies. 
  <ul>
  	<li>USDCHF - the cost of $1 in Swiss Francs.</li>
    <li>USDJPY - the cost of $1 in Japanese yens.</li>
    <li>EURUSD - the cost of Euro 1 in US dollars.</li>
    <li>GBPUSD - the cost of 1 GBP in US dollars. </li>
	</ul></p>
  <p><strong>T</strong>hat is, quotes are expressed in the units of the second currency for a unit of the first one. For example, quote USDJPY 108,91 shows that $1 costs 108,91 Japanese yens. Quote EURUSD 0.9561 shows that 1 Euro costs 0.9561 US dollars.</p>
  <p><strong>T</strong>he last figure in the quote is called &quot;pip&quot;. The cost of the pip is different for every currency, and depends on the leverage and current quote.</p>
  <p><strong>T</strong>he formula for calculating 1 pip is:</p>
  <p><strong>100,000/current quote  * K</strong></p>
  <p>where &#1050;=1 at leverage 1:100,<br />
    &#1050;=2 at leverage 1:200,<br />
    &#1050;=0,5 at leverage 1:50,<br />
    K=0,25 at leverage 1:25.</p>
  <p><strong>E</strong>xamples:</p>
  <p>USDJPY = 108.91 leverage 1:100<br />
    100.000 / 10891 &#1093; 1 = 9,18 USD</p>
  <p>EURUSD = 0.9561 leverage1:200<br />
    100.000 / 9561 &#1093; 2 =20,92 USD</p>
  <p>GBPUSD and EURUSD are <strong>direct quotes</strong>, i.e. when the chart goes up, GBP and EUR become more expensive, and when it goes down, the currencies become cheaper. USDCHF and USDJPY are <strong>backward quotes</strong>, and when the chart grows, prices on CHF and JPY fall, and when the chart goes down, the prices grow.</p>
  <p><strong>O</strong>n direct quotes you buy according to <strong>ASK</strong> and sell according to <strong>BID</strong>. With backward quotes, you buy according to <strong>BID</strong> and sell according to <strong>ASK</strong> .</p>
  <p><strong>T</strong>rading in the <strong>FOREX</strong> market is realized in lots. When you open a position, you can choose the number of lots you want from 1 to 10. One lot equals $ 100,000. The deposit sum for one lot will vary from $500 to $2000, depending on the credit leverage you choose. Leverage is a financial mechanism that allows crediting speculative transactions with a small deposit. We give you an opportunity to choose a credit leverage in the range of 1:200 to 1:25.</p>
</td>
</tr>
</table>

