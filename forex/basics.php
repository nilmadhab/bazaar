<p>  <strong>Foreign exchange</strong> (Forex or FX) is one of the most exciting, fast-paced markets around. Until recently, trading in the Forex market had been the domain of large financial institutions, corporations, central banks, hedge funds and extremely wealthy individuals. The emergence of the internet has changed all of this, and now it is possible for average investors to buy and sell currencies easily with the click of a mouse. </p>
<p>Daily currency fluctuations are usually very small. Most currency pairs move less than one cent per day, representing a less than 1% change in the value of the currency. This makes foreign exchange one of the least volatile financial markets around. Therefore, many speculators rely on the availability of enormous leverage to increase the value of potential movements. </p>
<p><strong> Terminology: </strong></p>
<p> Currency-pair: In Forex, you always come across a currency-pair and not a single currency. You can only buy or sell a pair.</p>
<p><ul>
  <li>Base Currency: The currency that you have to buy if you long a currency pair or sell if you short a currency pair. Example for USD/INR pair of currency, USD is the base currency.</li>
  <li>Buying a pair is referred to as &lsquo;Longing&rsquo;; selling is referred to as &lsquo;Shorting&rsquo;. So if you long a currency-pair it implies that you have to buy the base currency and short a currency pair implies that you have to sell the base currency.. </li>
  <li>There will be two different prices which the market fixes for the Currency-pair. These are: </li>
  <ul>
    <li>Ask Price &ndash; This is the price you will have to pay to long a currency-pair. </li>
    <li>Bid Price - This is the price you will get when you short a currency-pair. </li>
  </ul>
</ul></p>
<p> <strong>Lots</strong>: In the foreign exchange market, the fluctuations are very small. So to increase the quantum of the transaction, you will be dealing in lots. Example: In the game, 1 Lot = 1000 currency-pairs, for instance if you long 1 lot, it means that you are longing a 1000 currency-pairs. You always deal in bulks referred to as lots. &nbsp;</p>
<p> <strong>Leverage</strong>: It is a multiplying factor. It increases the amount of risk associated to each transaction. </p>
<p> <strong>Margin</strong>: It is the amount that you have to pay up if you long a pair or the amount that you will get on shorting a pair. In forex, margin is the minimum required balance to place a trade. When you open a forex trading account, the money you deposit acts as collateral for your trades. This deposit, called margin, is typically 1% of the value of the position . </p>
<p> <strong>Balance</strong>: It reflects your financial status after one complete round trip of a transaction i.e. after closing a particular currency pair. It will only change when you close a transaction. The profit or loss is added or subtracted respectively from the Balance after each round-trip. </p>
<p> <strong>Equity</strong>: It shows your instantaneous net worth. It will be your balance (financial status) if the transaction is closed just at that point of time taking into accounts the profit or loss that you make. </p>
<p> <strong>Percentage</strong>: It is calculated by dividing equity attained after closing the transaction with margin. To long or short a pair the percentage should not be less than 50. It provides a check so that the participant may not buy too many currency pairs. </p>
<p><strong> How to play?</strong> &nbsp;</p>
<p>We take an example. Suppose you choose to deal in USD to INR 39.50/5 (bid price =39.50, ask price= 39.55). </p>
<p><strong> Long </strong></p>
<p>You select 2 lots and a leverage of 100 and you decide to long the pair. </p>
<p>As you have to long the pair you have to pay the margin based on ask price at that point of time. This can also be viewed as buying the base currency at the ask price. Let this ask-price be 39.55 INR. </p>
<p>Now if you wish to close the transaction at this point itself then you will get back money on the basis of bid price. So your profit or loss is calculated on the basis of the difference between the ask price when you long the pair and bid price at that point of time when you close the transaction. This difference is multiplied with the no. of lots and the leverage. Now this is the profit or loss you would make in INR. However, we must convert it to USD ( all profit calculations should be in base currency i.e. USD) by dividing it by the bid price at the time of closing the transaction. This gives you the profit or loss on one complete transaction when you long your pair. </p>
<p><strong> Example: </strong></p>
<p> &nbsp;Initially let us suppose that at our balance= 2500000 USD, </p>
<p>equity= 2500000 USD </p>
<p>Suppose you choose to deal in USD to INR 39.50/5 (bid price =39.50, ask price= 39.55). </p>
<p>We want to long 2 lots of the above pair i.e. 2x1000 USD. </p>
<p>Let us take leverage as 1:100 </p>
<p>Let the ask-price = 39.55 INR and bid-price = 39.50 INR. </p>
<p>&nbsp; </p>
<p>Now margin= 1% of no. of lots x 1000 x leverage x ask-price </p>
<p>=1/100 x 2 x 1000 x 100 x 39.55 INR </p>
<p>=79100 INR </p>
<p>This is the amount we have to pay as margin. </p>
<p>Now balance remains the same. (Note that the balance changes only in a round transaction). Equity will change and will reflect our balance (financial status) if we were to close the deal at the instant itself. </p>
<p>Now if we wish to close the transaction just after we opened it, the ask price and bid price remain the same. (bid price =39.50, ask price= 39.55). </p>
<p>So our profit in USD= no. of lots x 1000 x leverage x (bid price &ndash; ask price)/ bid price </p>
<p>= 2 x 1000 x 100 x ( 39.50 &ndash; 39.55)/39.50 </p>
<p>= - 253.16 USD </p>
<p>Now new balance= 2500000 &ndash; 253.16 USD</p>
<p>= 2499746.84 USD </p>
<p>Equity = Balance = 2499746.84 USD </p>
<p>&nbsp; </p>
<p><strong> Short </strong></p>
<p> Consider again the pair: </p>
<p>Suppose you choose to deal in USD to INR 39.50/5 (bid price =39.50, ask price= 39.55). </p>
<p>Suppose you select 2 lots and a leverage of 1:100 and you decide to short the pair.</p>
<p>As you have to short the pair you have to sell the base currency i.e. USD in this case, at the bid price. Let this bid price be 39.50 INR. For understanding, view it as if you sold USD and acquired INR at a conversion rate of 39.5 </p>
<p>Now if you wish to close the transaction at some point of time, then you will have to sell the INR to get back dollars at a certain conversion rate. This conversion rate shall be the ask price at the time of closing the transaction. Hence your profit or loss is calculated on the basis of the difference between the bid price when you short the pair and ask price when you want to close the transaction. Again, this difference is multiplied with the no. of lots and the leverage. Now this is the profit or loss you would make in USD, so it does not need any conversion. This gives you the profit or loss on one complete trip of the transaction . This would be clear with the help of an example: </p>
<p><strong> Example: </strong></p>
<p> Initially let us suppose that at our balance= 2500000 USD, </p>
<p>equity= 2500000 USD </p>
<p>Suppose we are dealing with USD-INR pair.. </p>
<p>We want to short 2 lots of the above pair i.e. 2x1000 USD. </p>
<p>Let us take leverage as 1:100 </p>
<p>Let the ask-price = 39.55 INR and bid-price = 39.50 INR. </p>
<p>Now balance remains the same. Equity will change and will reflect our balance (financial status) if we were to close the deal at the instant. </p>
<p>Now if we wish to close the transaction at the same ask price and bid price. </p>
<p>Now since we were dealing in selling our base currency i.e. USD, so the profit remains in USD and hence needs no conversion.</p>
<p>So our profit in USD= no. of lots x 1000 x leverage x (bid price &ndash; ask price) </p>
<p>= 2 x 1000 x 100 x ( 39.50 &ndash; 39.55)</p>
<p>= - 10000 USD </p>
<p>Now balance= 2500000 &ndash; 10000</p>
<p>= -2490000USD </p>
<p>Equity = Balance = -2490000 USD &nbsp; </p>
<p>&nbsp; </p>
<p><strong> Why the profit shown just after opening a position always negative? </strong></p>
<p>The moment you open a transaction, your profit shown is negative because the ask price of a currency pair is always more than the bid price at an instance of time. So if you long a pair means that you buy the base currency at a higher price. Hence if you wish to close the transaction as soon as you buy it, your profit shown will be negative because the bid price at the time of closing the position shall always be less than the ask price at the point of opening the position in this case.</p>
<p><strong>How to earn money? </strong></p>
<p>To make profit in Forex you have to wait till your bid price at a point of time becomes greater than the ask price at which u bought your base currency in case you long a pair. In case of short you would make profit if the ask price at a point of time becomes less than the bid price at which you sold the base currency.</p>
<p><b>Commodity Trading</b><br />Commodity markets are markets where raw or primary products are exchanged. These raw commodities are traded on regulated commodities exchanges, in which they are bought and sold in standardized contracts.The trading of commodities consists of direct physical trading and derivatives trading.<br />
Trading on exchanges in China and India has gained in importance in recent years due to their emergence as significant commodities consumers and producers. </p>
<p>&nbsp; </p>
<p align="center"><strong> Happy Trading! </strong></p>

