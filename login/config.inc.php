<?php

# Check "docs/configurating.txt" for a more complete description of how you
# set the different settings and what they will do.

# PHPFM's home directory
# Use forward slashes instead of backslashes and remeber a traling slash!
$home_directory         = "./";

# Language of PHPFM.
$language       = "english";

# Session save_path information
# *NIX systems      - set this to "/tmp/";
# WINDOWS systems   - set this to "c:/winnt/temp/";
# NB! replace "c:/winnt/" with the path to your windows folder
#
# Uncomment _only_ if you are experiencing errors!
# $session_save_path    = "/tmp/";

# Login configuration
$phpfm_auth     = TRUE;
$username       = "root";
$password       = "";

# Access configuration
# Each variable can be set to either TRUE or FALSE.
$AllowCreateFile        = TRUE;
$AllowCreateFolder      = TRUE;
$AllowDownload          = TRUE;
$AllowRename            = TRUE;
$AllowUpload            = TRUE;
$AllowDelete            = TRUE;
$AllowView              = TRUE;
$AllowEdit              = TRUE;

# Icons for files
$IconArray = array(
     "text.gif"       => "txt ini xml xsl ini inf cfg log nfo",
     "layout.gif"     => "html htm shtml htm pdf",
     "script.gif"     => "php php4 php3 phtml phps conf sh shar csh ksh tcl cgi pl js",
     "image2.gif"     => "jpeg jpe jpg gif png bmp",
     "c.gif"          => "c cpp",
     "compressed.gif" => "zip tar gz tgz z ace rar arj cab bz2",
     "sound2.gif"     => "wav mp1 mp2 mp3 mid",
     "movie.gif"      => "mpeg mpg mov avi rm",
     "binary.gif"     => "exe com dll bin dat rpm deb",
);

# Files that can be edited in PHPFM's text editor
$EditableFiles = "php php4 php3 phtml phps conf sh shar csh ksh tcl cgi pl js txt ini html htm css xml xsl ini inf cfg log nfo bat";

# Files that can be viewed in PHPFM's image viewer.
$ViewableFiles = "jpeg jpe jpg gif png bmp";

# Format of last modification date
$ModifiedFormat = "H:i m-d-Y";

# Zoom levels for PHPFM's image viewer.
$ZoomArray = array(
     5,
     7,
     10,
     15,
     20,
     30,
     50,
     70,
     100,       # Base zoom level (do not change)
     150,
     200,
     300,
     500,
     700,
     1000,
);

# Hidden files and directories
$hide_file_extension       = array(
                                    "foo",
                                    "bar",
                             );

$hide_file_string          = array(
                                    ".htaccess",
                             );

$hide_directory_string     = array(
                                    "secret dir",
                             );

$MIMEtypes = array(
     "application/andrew-inset"       => "ez",
     "application/mac-binhex40"       => "hqx",
     "application/mac-compactpro"     => "cpt",
     "application/msword"             => "doc",
     "application/octet-stream"       => "bin dms lha lzh exe class so dll",
     "application/oda"                => "oda",
     "application/pdf"                => "pdf",
     "application/postscript"         => "ai eps ps",
     "application/smil"               => "smi smil",
     "application/vnd.ms-excel"       => "xls",
     "application/vnd.ms-powerpoint"  => "ppt",
     "application/vnd.wap.wbxml"      => "wbxml",
     "application/vnd.wap.wmlc"       => "wmlc",
     "application/vnd.wap.wmlscriptc" => "wmlsc",
     "application/x-bcpio"            => "bcpio",
     "application/x-cdlink"           => "vcd",
     "application/x-chess-pgn"        => "pgn",
     "application/x-cpio"             => "cpio",
     "application/x-csh"              => "csh",
     "application/x-director"         => "dcr dir dxr",
     "application/x-dvi"              => "dvi",
     "application/x-futuresplash"     => "spl",
     "application/x-gtar"             => "gtar",
     "application/x-hdf"              => "hdf",
     "application/x-javascript"       => "js",
     );