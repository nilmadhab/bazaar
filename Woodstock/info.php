<!-- code and css by PRASHANT for giving the detailed stats of all the  stocks -->
<style type = "text/css" >
table a, table, tbody, tfoot, tr, th, td, table caption {
	font-family: Verdana, arial, helvetica, sans-serif;
	background:#262b38;
	color:#fff;
	
	font-size:12px;
}
table, table caption {
	border-left:3px solid #567;
	border-right:3px solid #000;
}
table {
	border-top:1px solid #567;
	border-bottom:3px solid #000;
}
table a {
	text-decoration:underline;
	font-weight:bold;
}
table a:visited {
	background:#262b38;
	color:#abc;
}
table a:hover {
	text-decoration:none;
	position:relative;
	top:1px;
	left:1px;
}
table caption {
	border-top:3px solid #567;
	border-bottom:1px solid #000;
	font-size:20px;
	font-weight:bold;
}
table, td, th {
	margin:0px;
	padding:0px;
}
tbody td, tbody th, tbody tr.odd th, tbody tr.odd td {
	border:1px solid;
	border-color:#567 #000 #000 #567;
}
td, th, table caption {
	padding:5px;
	vertical-align:middle;
}
tfoot td, tfoot th, thead th {
	border:1px solid;
	border-color:#000 #567 #567 #000;
	font-weight:bold;
	white-space:nowrap;
	font-size:14px;
}
</style>
<?php
include("connection.php");
	$check_q="select * from stocks_rate";
	$check_r=mysql_query($check_q) or die("Could not process Request");
	$check_cnt=mysql_num_rows($check_r);
	
	if($check_cnt==0)
	{
		die('No stocks exist');
	}
	echo "<table border='1'>";
	?>
	<th>Stock</th>
	<th>Opening Price</th>
	<th>Last traded</th>
	<th>Percent change</th>
	<?php
	while($check_a=mysql_fetch_array($check_r)){
	    $open = $check_a['open'];
		$per = ($check_a['price']-$open)/($open);
		$per = $per*100;
		echo "<font color='#AA9F00'>";
		echo "<tr><td >";
	    echo $check_a['stock'];
		echo "</td><td align='right'>";
		echo $check_a['open'];
		echo "</td><td align='right'>";
		echo $check_a['price'];
		echo "</td><td align='right'>";
		echo "</font>";
		if ($per>=0){
		echo "<font color='#18f60d'>";
		}
		else {
		echo "<font color='#f43212'>";
		}
		echo round($per,2);
		echo " %";
		echo " &nbsp";
		echo " &nbsp";
		echo "</font>";
		echo "</td></tr>";
		}
?>
