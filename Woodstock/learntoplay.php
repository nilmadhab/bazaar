<div id="myspan">
<font size="-1">
<div id="heading">What's a Stock/Share?</div>
<p>
 A stock is a certificate, that certifies one's ownership in a company.
If there are in total 10,00,000 shares issued by a company, owning one stock means the ownership of 1/10,00,000 part of the company. It includes the ownership in the value of the comapy.In simple terms, it is a partnership between a lot of investors, in the ratio of their stock holdings.</p>
<p>The ownership of a stock does not mean that one gets a say in the working of the company on a day to day basis, but that one has the right to cast the vote in the Annual General Meeting where the company decides on the major issues and on the way the company is to be run.
</p>
<p>The stock could be of two kinds, Common and Preference . A preference stockholder gets a fixed dividend, no matter whether the companies makes profit or not! Also, in the case of liquidation, they get a right to the assets before the Common shareholders. To a reader it might seem stupid to buy a Common stock, but then because the dividend is fixed, the share price is also generally fixed!!! thus the preference stock holder does not get much return(Golden Rule: No risk, No return!!) . Actually many consider preference stock to be a kind of bank deposit. Note that the share price of common stock is no way related to that of the preference stock
</p>
<p>More often than not a small investor will only trade or invest in normal shares. So, throughout this tutorial, we will deal with Common stocks and not Preference Stock, unless specified otherwise.

 </p>
 </p>

<div id="heading"> Trading of Stocks</div>
<p>The stock market/Exchange is a place where all the transactions are made. (Image: The Bombay Stock Exchange)
</p><p>
In India, to trade in stocks, u need to get a demat account (In most developed markets, Demat account is compulsory) These accounts are like bank accounts, the diffence being that your bank account holds money, while a demat account holds Stocks. This makes sure that u dont need to keep the share certificates safely. There is no tension of lost certificates. Also it makes the business much faster with deliveries directly from one account to another in a matter of one or two days. Just 15 years back one single transaction could take as many as 10 days to complete.
</p>
<p>
You also need to get in touch with a broker in your locality. A broker is geneally a person whom you can trust and who could trust you. Otherwise, the broker would like to have some advance deposited with him before he does some transaction on your behalf. With technology, over the last 20 years the role of a broker has become much more confined. Earlier he used to find a buyer for a seller and vice-versa, now he just needs to sit in fornt of his computer and technology does the rest.</p><p>
The role of the stock market has remained as important as it was 100 years back, except that the transactions can take place through the internet without the physical presence of the broker. They have become much more sophisticated with different kinds of trades, specially the futures and options and stock Borrowing(Stock borrowing is still not allowed in India while futures and options are allowed in a selected few stocks)
</p><p>
You just need to call your broker and ask him to make a transaction in a particular script on your behalf. One is required to pay a Brokerage(generally between 0.45 to 1%) to the broker depending on the volume of business that you give to the broker. This is mutually negotiated at the time a sharetrader enters the business.
</p>
<div id="heading"> The Share Price</div>
<p>Share price is the single most important thing that makes a shareholder jump, dance, run cry, laugh, commit suicide or whatever. It is therefore apt to discuss this first.
  
Most simply, (and perhaps most efficiently) if the market(the overall share investors) values a company at Rs. 10,00,00,000 and the number of issued shares are 10,00,000 then the share price will be 10,00,00,000/10,00,000 = Rs. 100. </p>
  <p>This said, is not necessarily the prevailing market price. The price fluctuates on every news and every event that directly or indirectly affects the <!--<a href="Chart.jpg"><img border="0" src="Chart.jpg" alt="" style="margin: 0px 0px 10px 10px; float: right;"/></a>-->company's earnings.
    
Another way is the normal demand-supply arguement that applies to all commodities. Shares of a company are treated as a commodity and the changes in the price depends on the demand-supply scenario at a particular time.
    
Share price is the direct function of the earnings of the company and many many other factors. These factors could run in 3 digits if ennumerated. I will try to list some important ones in a later post.
    
One thing that is necessary to understand is that even the management might not be able to value its own company properly, forget analysts and share holders!!! Sometimes the Projected earnings  by different analysts vary by more than 100%  at a point in time!!! And hence the target price for a time period could be tough to make. </p>
  <p>Does this mean that there is no hope in the markets unless one gambles? The answer is that the risks are always there, even in the best companies, but these can be minimised by 'Portfolio Diversification' and by 'Systematic Investment Plans'. Not just minimised but made good money from. We will try to discuss these topics after we have covered some of the more basic topics.
  </p>
<div id="heading"> Brockerage</div>
<p>Brockerage is relatively a new concept.
  
Most simply, (and perhaps most efficiently) It is the amount of money you have to pay for the execution of the order. </p>
  <p>It is a percentage which depends on the number of transactions you carry out. So if the number of transactions are more, you will see that the brockerage we take also increases.
    Here it is generally 0.1% per share for first 100 transactions, 0.2% per share for next 900 transactions, and 0.5 % per share for any number of transactions above 1000.
This helps in effectively reducing the number of transactions carried out by a person, hence limiting the number of gambles. 
 </p>
  
<div id="heading">Tables and Tickers</div>
 <p> All Financial Papers have a table showing the shares traded along with a few columns besides them.</p>
  <p>  More often than not, the newpaper will carry a small header/footer explaining the relevant details. The most important ones are Open, High, Low, Close, Volume. Apart from all these they also carry one term called <strong>P/E</strong> ratio. This is the foundation for the understanding of the stock valuations. </p>
  <p>We have omitted this one because We have not dealt with this topic yet.</p>
  <p><strong>Open</strong>: Trade at which First trade of the day takes place.</p>
  <p><strong>High</strong>: Highest price during the day. </p>
  <p><strong>Low</strong>: Lowest price during the day. </p>
  <p><strong>Close</strong>: Weighted Avg Price of the last few minutes of trade. (difft. for difft. stock markets) ... This is not the last traded price!</p>
  <p><strong>Volume</strong>: Total shares traded during the day. </p>
  <p> </p>
  <p>The TV channels show a strip, which shows the last traded price during the market hours, delayed by a few minutes(usually 2-15 minutes). In the off market hours, they carry the  closing price of the stock. </p>
  <p>They TV channels will show an uptick or downtick specifying the change in price during the day. </p>
  <p>   </p>
  <div id="heading">Fast Forward!!</div>
  <p> This one can be skipped!!!!</p>
  <p>We are posting this one just for fun. </p>
  <p>Specially those who have been tracking some financial papers and some of the financial channels. How many of these terms you have heard of and how many you understand exactly.(Don't worry even if you dont understand a single topics here, actually these are the terms we will be discussing in the time to come!!)</p>
  <p><strong>EPS</strong>, <strong>PE</strong>, <strong>PEG</strong>, <strong>GDP</strong>, <strong>Debt</strong>, <strong>Equity</strong>, <strong>Merger</strong>, <strong>Demerger</strong>, <strong>Registrar</strong>, <strong>Face Value</strong>, <strong>Buy Back</strong>, <strong>Bonus</strong>, <strong>Rights</strong>, <strong>Dividend</strong>, <strong>OPM</strong>, <strong>NPM</strong>, <strong>Mutual Funds</strong>, <strong> Dividend Yield</strong>,<strong> Bull</strong>, <strong>Bear</strong>, <strong>Smart money</strong>, <strong>ULIP</strong>, <strong>SIP</strong>, <strong>LTCG tax</strong>, <strong>Charts</strong>, <strong> Insider trading</strong>, <strong>private placement</strong>, <strong>IPO</strong>, <strong>QIB</strong>, <strong>Bonds</strong>, <strong>ROCE</strong>,<strong> RONW</strong>,<strong> ROI</strong>,<strong> Beta value</strong>, <strong>Annual Report</strong>, <strong>Put</strong>, <strong>Call</strong>, <strong>Short</strong>, <strong>Cover</strong>, <strong>Margin</strong>, <strong>Badla</strong>, <strong>Derivatives</strong>, <strong>Futures</strong>, <strong>Options</strong>.</p>
  <p>If we have got your blood pumping faster, then we have struck the right chord. If u understand each of these terms properly, this may not the best place for you to try and improve your understanding of the markets! </p>
  <p> </p>
  <div id="heading">The Index (Sensex/Nifty)  </div>
   <p> Because there are more than 5000 stocks listed and traded on the Indian Stock Markets, it is in general not possible to say what the overall market movement was, on a particular day. One might suggest to take the advance/decline ratio (ie the number of stocks that increased to those decreased) But it is very much possible that most of the stocks that increased on a particular were traded in a very small quantity (say 1,000 shares on an average), though the trades on the shares whose value decreased were traded in a large quantity(say an average of 1,00,000 shares). In this case we would have jumped to a wrong conclusions. <br/>
    <br/>
So, the Indices were created. These indices generally cover 30-500 shares. In India the Sensex has 30 shares and Nifty has 50. Each stock is given a weightage, which may change.(though very slightly) In most of the cases, the weightage is dependent on the market capitalisation(Actually Free Float is used which is slightly different from market capitalisation) of the individual stock. The weighted sum of the share prices of these shares give the value of sensex. (Sometimes there is a multiplicative factor multiplied to the final weighted average as well) <br/>
<br/>
The way in which the stocks are chosen for a particular index depends on the market cap and 'beta value'(related to standard deviation in the share price). Also, sometimes sectoral weightage is also kept in mind before deciding the final list. </p>
</font>

<div id="heading">Short Selling</div>
<font size="-1"><p> Everyone makes money when market goes up (Does everyone make money ?) ... Does everyone lose money when the market goes down??? </p>
  
  <p>At the first sight it might seem that atleast no one loses money when the market goes up.. and no one makes money when the market goes down!!!!!</p>
  
  <p>This is very much untrue.... </p>
  <p>You can make money when the market is going down as well! Welcome "<strong>Short selling</strong>"! </p>
  <p><strong>Short Selling</strong>: Act of selling shares (even used for commodities and other things) without owning them. This is done in anticipation of a price fall. One sells the stock at the current market price, hoping a price crash. If the prices actually crash, he buys back (this act is called <strong>covering shorts</strong>) from the markets at a later date at a lower price, thus making a profit on the transaction.</p>
  <p>Short selling is effectively buying negetive no. of stocks. </p>

  <p>There is more to short selling, but it may make things a bit complex specially for those who have never traded in the stock markets. So we have tried to put things simple and straight. More details about short selling will be given in a later (more advanced) tutorial.</p>
  </font>

<div id="heading">Trigger Price</div>
<font size="-1"><p>Trigger price is the price, which if touched, activates an order.</p>
<p>Suppose you are long on a stock at 102. You don't want a loss of more than Rs. 1. So you put a stop loss sell order of Rs 101 with a trigger price of Rs 101.05.<br />
  What the system (exchange) does is - it checks if the LTP (Last Traded Price) is less than or equal to Rs 101.05. As soon as the condition is met, the exchange will put your Rs 101 sell order in the normal order book.<br />Unless the LTP goes below or is equal to the trigger price, a stop loss trigger sell order is not activated. Similarly unless the LTP goes above or is equal to the trigger price, a stop loss trigger buy order is not activated.</p>
<p>Thus a trigger order has two components - trigger price and the price at which the order is placed.<br />A trigger order may also be a market order. This means if triggered, the trade will take place at market price.</p>
</font>

<div id="heading">Futures Trading</div>
<font size="-1"><p>Futures is a contract or an agreement between two parties: a short position - the party who agrees to deliver a commodity -  and a long position - the party who agrees to receive a commodity. In every futures contract, everything is specified: the quantity and quality of the commodity, the specific price per unit, and the date and method of delivery. The &ldquo;price&rdquo; of a futures contract is represented by the agreed-upon price of the underlying commodity or financial instrument that will be delivered in the future.<br />
  Due to its highly competitive nature, the futures market has become an important economic tool to determine prices based on today's and tomorrow's estimated amount of supply and demand. Futures markets are also a place for people to reduce risk when making purchases. Risks are reduced because the price is pre-set, therefore letting participants know how much they will need to buy or sell.</p>
  <p><b>Profit and Loss</b><br>
  The profits and losses of a futures contract depend on the daily movements of the market for that contract and are calculated on a daily basis. Unlike the stock market, futures positions are settled on a daily basis, which means that gains and losses from a day's trading are deducted or credited to a person's account each day. In the stock market, the capital gains or losses from movements in price aren't realized until the investor decides to sell the stock or cover his or her short position. <br />
  For example, say the futures contracts for wheat increases to $5 per bushel the day after a farmer and a bread maker enter into their futures contract of $4 per bushel. The farmer, as the holder of the short position, has lost $1 per bushel because the selling price just increased from the future price at which he is obliged to sell his wheat. The bread maker, as the long position, has profited by $1 per bushel because the price he is obliged to pay is less than what the rest of the market is obliged to pay in the future for wheat. <br />
</p>  
</font>

<div id="heading">Basic Trades of traded Stock Options</div>
<img src="http://www.ktj.in/events_images/woodstock/options3.png" style="float:right" style="padding-left:10px" />
<font size="-1"><p><b>Long Call</b> : A share trader who believes that a stock's price will <strong>increase</strong> might buy the right to purchase the stock (a call option) rather than just purchase the stock itself. He would have no obligation to buy the stock, only the right to do so until the expiration date. If the stock price at expiration is above the exercise price by more than the premium (price) paid, he will profit. If the stock price at expiration is lower than the exercise price, he will let the call contract expire worthless, and only lose the amount of the premium. A trader might buy the option instead of shares, because for the same amount of money, he can control (leverage) a much larger number of shares.</p><br />
<img src="http://www.ktj.in/events_images/woodstock/options4.png" style="float:right" style="padding-left:10px" />
<p><b>Long Put</b> : A trader who believes that a stock's price will <strong>decrease</strong> can buy the right to sell the stock at a fixed price (a put option). He will be under no obligation to sell the stock, but has the right to do so until the expiration date. If the stock price at expiration is below the exercise price by more than the premium paid, he will profit. If the stock price at expiration is above the exercise price, he will let the put contract expire worthless and only lose the premium paid.</p><br />
<img src="http://www.ktj.in/events_images/woodstock/options1.png" style="float:right" style="padding-left:10px" />
<p><b>Short Call</b> : A trader who believes that a stock price will <strong>decrease</strong>, can sell the stock short or instead sell, or "write," a call. The trader selling a call has an obligation to sell the stock to the call buyer at the buyer's option. If the stock price decreases, the short call position will make a profit in the amount of the premium. If the stock price increases over the exercise price by more than the amount of the premium, the short will lose money, with the potential loss unlimited.</p><br />
<img src="http://www.ktj.in/events_images/woodstock/options2.png" style="float:right" style="padding-left:10px" />
<p><b>Short Put</b> : A trader who believes that a stock price will <strong>increase</strong> can buy the stock or instead sell a put. The trader selling a put has an obligation to buy the stock from the put buyer at the put buyer's option. If the stock price at expiration is above the exercise price, the short put position will make a profit in the amount of the premium. If the stock price at expiration is below the exercise price by more than the amount of the premium, the trader will lose money, with the potential loss being up to the full value of the stock.</p>
</div>