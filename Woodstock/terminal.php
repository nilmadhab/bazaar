<?php
	include_once('session_check.php');
	include_once('connection.php');
	include_once('admin/price.php');
	include_once('func.php');
	include('parameters.php');
	$nameval['Buy']="Sell";
	$nameval['Short Sell']="Cover Short";


?>
<BR>

<div id="targetTab">

<div id="myspan">
 <h1>Stock Holdings</h1>

<table width="100%" id="brownBorder" cellpadding="5" cellspacing="0">

<tr><td id="brownBorder" align='center'><b>Company</b>
</td><td id="brownBorder" align='center'><b>B or SS</b></td>
<td id="brownBorder" align='center'><b>Quantity in stock</b></td>
<td id="brownBorder" align='center'><b>Quantity to sell</b></td>
<td id="brownBorder" align='center'><b>Current Price</b></td>
<td id="brownBorder" align='center'><b>Pending Price</b></td>
<td id="brownBorder" align='center'><b>Action</b></td></tr>

<?php

	
	$totalstockamt['Buy']=0;

	$totalstockamy['Short Sell']=0;

	$query="select * from wud_opened_options where userid='".$_SESSION['userid']."' order by stockname asc";

	$result=mysql_query($query);

	while($arr=mysql_fetch_array($result))

	{
		//temp by salil
		//echo $arr['stockname'];
		$stck = explode('.',$arr['stockname']);
		$stock=strtoupper($stck[0]);
		//echo $stock." ";
		$cp=$$stock;

		$current_price=round($cp,2);

		$totalstockamt[$arr['bors']]=$totalstockamt[$arr['bors']]+($current_price*$arr['quantity']);

		print("<tr>");
		
		print("<td id='brownBorder'><input type='button'  class='buttonStyle' 
		 name='".urlencode($stock)."' value='".$arr['stockname']."' onclick='javascript:getStats(this.value);'/></td><td id='brownBorder'>".$arr['bors']."</td><td id='brownBorder'>".$arr['quantity']."</td><td id='brownBorder'  align='center'><input type='text' size='5' name='quantity' id='quantity".$arr['stockname']."' value='".$arr['quantity']."'></td><td id='brownBorder' align='center'>".$current_price."</td><td id='brownBorder' align='center'><input type='text' size='5' name='pendingprice".$arr['stockname']."' id='pendingprice".$arr['stockname']."' value=''></td><td id='brownBorder' align='center'>");

		print("<input type='hidden' name='orderno' id='orderno".$arr['stockname']."' value='".$arr['orderno']."'>");

		print("<input type='hidden' name='bors' 
			id='bors".$arr['stockname']."' value='".$arr['bors']."'>");

		print("<input type='hidden' name='stockname' id='stockname".$arr['stockname']."' value='".$arr['stockname']."'>");

		print("<input type='hidden' name='oldquantity' id='oldquantity".$arr['stockname']."' value='".$arr['quantity']."'>");

		print("<input type='hidden' name='sellprice' id='sellprice".$arr['stockname']."' value='".$currentprice."'>");

		print("<input type='hidden' name='tranprice' id='opendateandtime".$arr['stockname']."' value='".$arr['trandateandtime']."'>");

		if($tradeallow==1)

		{

		print("<input type='submit' name='submit' value='".$nameval[$arr['bors']]."' onclick=	\"javascript:

	{

		var string=document.getElementById('quantity".$arr['stockname']."').value;

		var string2=document.getElementById('pendingprice".$arr['stockname']."').value;

		var ok=1;

		var ok1=1;

		for (var j=0;j < string2.length;j++)

        if ((string2.substring(j,j+1) < '0') || (string2.substring(j,j+1) > '9'))

			{

				if(string2.substring(j,j+1)!='.')

				{

					ok1=0;

				}

			}

    if (string.length == 0)

		{

			ok=0;

		}

    for (var i=0;i < string.length;i++)

        if ((string.substring(i,i+1) < '0') || (string.substring(i,i+1) > '9'))

		{

			ok=0;

		}

	

	ok=0;

	for (var i=0;i < string.length;i++)

        if ((string.substring(i,i+1) > '0'))

		{

			ok=1;

		}	

		

	if(parseInt(string)>parseInt(document.getElementById('oldquantity".$arr['stockname']."').value))

	{

		ok=0;

	}

	if(parseInt(string)<1)

	{

		ok=0;

	}

	if(ok1==1)

	{

	if(ok==1)

		{

			get1('".$arr['stockname']."');

		}

	else

		{	

			alert('Please enter a valid quantity');

		}

	}

	else

	{

		alert('Invalid pending price quantity');

	}

	}

	\" class='buttonStyle' >");

	}

		print("</td>");

		print("</tr>");

		

	

	}

?>

</table>
<br /><br />



<h1>Pending sell/short cover</h1>

<table width="100%" id="brownBorder" cellpadding="5" cellspacing="0">

<tr><td id="brownBorder" align='center'><b>Company</b></td><td id="brownBorder" align='center'><b>B or SS</b></td><td id="brownBorder" align='center'><b>Quantity to sell</b></td><td id="brownBorder" align='center'><b>Current Price</b></td><td id="brownBorder" align='center'><b>Pending Price</b></td><td id="brownBorder" align='center'><b>Action</b></td></tr>

<?php

	$totalpendingamt['Buy']=0;

	$totalpendingamt['Short Sell']=0;

	$query="select * from wud_pending_close_options where userid='".$_SESSION['userid']."' order by stockname asc";

	$result=mysql_query($query);

	while($arr=mysql_fetch_array($result))

	{
		$stck = explode('.',$arr['stockname']);
		$stock=strtolower($stck[0]);

		$cp=$$stock;

		$current_price=round($cp,2);

		$totalpendingamt[$arr['bors']]=$totalpendingamt[$arr['bors']]+($current_price*$arr['quantity']);
		

		print("<tr>");

	print("<td id='brownBorder' >".$arr['stockname']."</td><td id='brownBorder'>".$arr['bors']."</td><td id='brownBorder'  align='center'>".$arr['quantity']."</td><td id='brownBorder' align='right'>".$current_price."</td><td id='brownBorder' align='center'>".$arr['pendingprice']."</td><td id='brownBorder' align='center'>");

		print("<input type='hidden' name='cancelorderno' id='cancelorderno".$arr['stockname']."' value='".$arr['orderno']."'>");

		print("<input type='hidden' name='cancelstockname' id='cancelstockname".$arr['stockname']."' value='".$arr['stockname']."'>");

		print("<input type='hidden' name='cancelbors' id='cancelbors".$arr['stockname']."' value='".$arr['bors']."'>");

		if($tradeallow==1)

		{

		print("<input type='submit' name='submit' value='Cancel' onclick=	\"javascript:

		{

					cancelpendingorder('".$arr['stockname']."');

		}

	\" class='buttonStyle' >");

		}

		print("</td>");

		print("</tr>");


	}

	

?>

</table>



<?php

	$up_q="select cashbalance,reserve from wud_rankings where userid='".$_SESSION['userid']."'";

	$up_r=mysql_query($up_q) or die($up_q);

	while($up_arr=mysql_fetch_array($up_r))

	{

		$m_amt=$up_arr['cashbalance']-$up_arr['reserve'];

	}

	$addeq=$totalstockamt['Buy']-$totalstockamt['Short Sell']+$totalpendingamt['Buy']-$totalpendingamt['Short Sell']+3*$m_amt;

	$q="update wud_rankings set equity=reserve+".$addeq." where userid='".$_SESSION['userid']."'";

	mysql_query($q) or die($q);

?>



<br /><br />

<h1>Pending Buy/Short sell</h1>

<table width="100%" id="brownBorder" cellpadding="5" cellspacing="0">

<tr><td id="brownBorder" align='center'><b>Company</b></td><td id="brownBorder" align='center'><b>B or SS</b></td><td id="brownBorder" align='center'><b>Quantity requested</b></td><td id="brownBorder" align='center'><b>Current Price</b></td><td id="brownBorder" align='center'><b>Pending Price</b></td><td id="brownBorder" align='center'><b>Action</b></td></tr>

<?php

//pending orders

$query="select * from wud_pending_options where userid='".$_SESSION['userid']."'";

	$result=mysql_query($query);

	

	while($arr=mysql_fetch_array($result))

	{

	print("<tr><td id='brownBorder'>".$arr['stockname']."</td><td id='brownBorder'>".$arr['bors']."</td><td id='brownBorder'>".$arr['quantity']."</td><td id='brownBorder'>".current_price($arr['stockname'],$arr['bors'])."</td><td id='brownBorder'>".$arr['pendingprice']."</td><td id='brownBorder'>");

		print("<input type='hidden' name='buypendingid".$arr['stockname']."' id='buypendingid".$arr['stockname']."' value='".$arr['pendingid']."'");

		print("<input type='hidden' name='buystockname".$arr['stockname']."' id='buystockname".$arr['stockname']."' value='".$arr['stockname']."'");

		if($tradeallow==1)

		{

print("<input type='button' name='submit' value='Cancel' onclick=	\"javascript:

		{

					cancelbuyorder('".$arr['stockname']."');

		}

	\" class='buttonStyle' >");



		}

	

	}
	
	

//

?>

</table>
</div></div>
<script type="text/javascript" src=" http://www.google-analytics.com/urchin.js "></script>
<script type="text/javascript">
_uacct = "UA-2839740-3";
urchinTracker();
</script>

