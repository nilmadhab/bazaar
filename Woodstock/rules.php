<font size='-1'>
<div id="heading">Rules</div>
      <p>&#149;&nbsp; Only top 30 players will be allowed to play from 17th of january.</p>
	  <p>&#149;&nbsp; You cannot have more than 10,000 stocks of a company at a time.</p>
	  <p>&#149;&nbsp;  You need to pay a small amount of margin for short transactions in options.</p>
		<p>&#149;&nbsp; You can close your opened transactions at least after 30 minutes of opening the transaction.</p>
        <p>&#149;&nbsp; Your college/school Identity card is mandatory while claiming prizes. It has to bear the same name and details as filled in the registration form. Those players whose form details do not match those of the college will be disqualified. </p>
        <p>&#149;&nbsp; You should have no source of income except in form of stipend or scholarships. You should not be a Professional. </p>
        <p>&#149;&nbsp; If you don't access your account for over 1 month, it may be deleted/ deactivated. </p>
        <p>&#149;&nbsp; The rules are subject to change at any point in time. </p>
        <p>&#149;&nbsp; The winner will be the person who has the maximum net worth at the end of the period. (The Kshitij Team holds the right to disqualify anyone without giving any reason) </p>
        <p>&#149;&nbsp; The Prizes are taxable. </p>
        <p>&#149;&nbsp; No member or relative of a member of the Kshitij's 'Woodstock family' is allowed to claim any prize. </p>
		<p>&#149;&nbsp; Net Worth may change on the expiry dates or at the end of the round even after the market is closed. </p>
		
        <p>&nbsp;</p>
</font>
