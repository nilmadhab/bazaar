<?php
	//Code by Tanuj : GUI for options transaction
	// Code modified wid introduction of divs.. by PRASHANT
	include('../session_check.php');
	include('../connection.php');
	include_once('../admin/price.php');
	include('../parameters.php');
	include('friday.php');
   
	if($tradeallow==2)
	{
		die("Woodstock is closed now.<br/>
			You cannot trade stock now but can only view your opened stock positions.
			You can play <a href='http://bazaar.ktj.in/forex'>forex</a> till 12 midnight.");
	}
	if($tradeallow==0)
	{
		die("Both Woodstock and Forex are closed now.<br>You cannot trade 
			stock now but can only view your opened stock positions.You can play
			 Woodstock from 10AM to 4PM only on working days.");
	}
	
	$query1="SELECT * FROM options_base ORDER BY stockid ASC";
	$result1=mysql_query($query1);
	
	$query3="SELECT * FROM options_base WHERE stock='ABAN.NS'";
	$result3=mysql_query($query3);
	$array3=mysql_fetch_array($result3);
	$spriceABAN=$array3['StrikePrice'];
	$spriceChunk=explode('~',$spriceABAN);
	$strike0=$spriceChunk[0];
	$strike1=$spriceChunk[1];
	$strike2=$spriceChunk[2];
	$strike3=$spriceChunk[3];
	$strike4=$spriceChunk[4];
	$strike5=$spriceChunk[5];
	$strike6=$spriceChunk[6];
	$strike7=$spriceChunk[7];
	$strike8=$spriceChunk[8];
	$strike9=$spriceChunk[9];
	$strike10=$spriceChunk[10];
	
?>
<script type="text/javascript" src="woodstock.js"></script>
<style type="text/css">
<!--
	.table {
	    width: 100%;
	    min-width: 640px;
	}

	.table2 {
	    width: 100%;
	    min-width: 640px;
	}

	.company {

		padding: .81%;
		float: left;
		border: thin none #0000FF;
		width: 18%;
		min-height: 28px;
		margin-left: 17%;
	}

	.select1 {
		padding: .81%;
		float: left;
		border: thin none #0000FF;
		width: 25%;
		min-height: 28px;
		min-width: 260px;
	}


	.pending {
		padding: .61%;
		float: right;
		border: thin none #0000FF;
		width: 17%;
		min-height: 28px;
		margin-right: .75%;
	}
-->
</style>
<div id="myspan"></div>
<h1 align="left">Options Trading</h1>
<br/><br/><br/><br/><br/>
<table width="45%" id="brownBorder" style="margin-left:20%;" cellpadding="5" cellspacing="0">
    	<tr>
		<td  id="brownBorder" valign="top"><b>Company</b></td>
		<td id="brownBorder" valign="top">
        	<select name="companyOptions" id="companyOptions" 
        	onChange="javascript:multipleOption(this.value)">
			  <?php   while($arr1=mysql_fetch_array($result1))
                        {
                            $name=$arr1["javaName"];
							$strike=$arr1["StrikePrice"];
							$nameChunk=explode('.',$name);
							$nameNew=$nameChunk[0];
                                                        
                            $query2="SELECT * FROM stocks_rate WHERE stock='$nameNew'";
                            $result2=mysql_query($query2);
                            $arr2=mysql_fetch_array($result2);
                            
                            $stockName=$arr2["stock"];
                            $price=$arr2["price"];
				?>
								<option value="<?php echo $name."~".$strike ; ?>"><?php echo $stockName."&nbsp;&nbsp;&nbsp;Rs.".round($price,2) ; ?></option>
							
                <?php
			  			}
		  		?>			
         	</select>
         </td></tr>

			
   		<tr><td id="brownBorder" valign="top"><b>Strike Price</b></td>
        <td id="brownBorder" valign="top">
		<select name="strikePrice" id="optionStrikePrice">
			<option id ="strike0" value="<?php echo $strike0 ; ?>"  selected>
				<?php echo $strike0 ; ?></option>
			<option id ="strike1" value="<?php echo $strike1 ; ?>">
				<?php echo $strike1 ; ?></option>
			<option id ="strike2" value="<?php echo $strike2 ; ?>">
				<?php echo $strike2 ; ?></option>
            <option id ="strike3" value="<?php echo $strike3 ; ?>">
            	<?php echo $strike3 ; ?></option>
            <option id ="strike4" value="<?php echo $strike4 ; ?>">
            	<?php echo $strike4 ; ?></option>
            <option id ="strike5" value="<?php echo $strike5 ; ?>"><?php echo $strike5 ; ?></option>
            <option id ="strike6" value="<?php echo $strike6 ; ?>"><?php echo $strike6 ; ?></option>
            <option id ="strike7" value="<?php echo $strike7 ; ?>"><?php echo $strike7 ; ?></option>
            <option id ="strike8" value="<?php echo $strike8 ; ?>"><?php echo $strike8 ; ?></option>
            <option id ="strike9" value="<?php echo $strike9 ; ?>"><?php echo $strike9 ; ?></option>
            <option id ="strike10" value="<?php echo $strike10 ; ?>"><?php echo $strike10 ; ?></option>
		</select>
		</td></tr>
    
 
   	<tr>	<td id="brownBorder" valign="top"><b>Call or Put</b></td>
        <td id="brownBorder" valign="top">
		<select name="cORp" id="cORp">
			<option id ="Call" value="Call"  selected>Call</option>
			<option id ="Put" value="Put">Put</option>
		</select>
		</td></tr>
     
   <tr>
   
   		<td id="brownBorder" valign="top"><b>Long or Short</b></td>
        <td id="brownBorder" valign="top">
		<select name="lORs" id="lORs">
			<option id ="Long" value="Long"  selected>Long</option>
			<option id ="Short" value="Short">Short</option>
		</select>
		</td></tr>
    
   <tr>
   		<td id="brownBorder" valign="top"><b>Expiry Date</b></td>
        <td id="brownBorder" valign="top">
		<select name="expiryDate" id="expiryDate">
			
						<option id ="friday1" value="<?php echo $friday1 ; ?>"  selected><?php echo $friday1 ; ?></option>
			
		</select>
		</td></tr>
   <tr>
   		<td id="brownBorder" valign="top"><b>Quantity</b></td>
        <td id="brownBorder" valign="top">
		<input type="text" name="oquantity" id="oquantity" value="" />
		</td>
		
		
  </tr>
    

</table>

 <div class="pending" style="margin-top:-170px;"><br /><br />
 	<input type="button"  class="buttonStyle" name="submit" style="cursor:pointer" 
 	value="Place Order" 
 	onclick="javascript:
	{
		var string=document.getElementById('oquantity').value;
		var ok=1;
    if (string.length == 0)
		{
			ok=0;
		}
    for (var i=0;i < string.length;i++)
        if ((string.substring(i,i+1) < '0') || (string.substring(i,i+1) > '9'))
			{
				ok=0;
			}
	
	if(ok==1)
		{
			option();
		}
	else
		{	
			alert('Please enter a valid quantity');
		}
	}
	" /></div>
<p>&nbsp;</p>

<h1 align="left">Options Stats</h1>
<div class="table2" align="center">
<div class="stats"><b>Select a company to view its available Options:</b>

<p>&nbsp;</p>
			<select name="companynameforoptions" id="companynameforoptions">
                		<?php  
					$query5="SELECT * FROM options_base ORDER BY stockid ASC";
					$result5=mysql_query($query5);
					
					while($arr5=mysql_fetch_array($result5))
                        		{
                            		$name=$arr5["javaName"];
						$nameChunk=explode('.',$name);
						$nameNew=$nameChunk[0];
                            
                            		$query6="SELECT * FROM stocks_rate WHERE stock='$nameNew'";
                            		$result6=mysql_query($query6);
                            		$arr6=mysql_fetch_array($result6);
                            
                            		$stockName=$arr6["stock"];
                            		$price=$arr6["price"];
			      ?>
						<option value="<?php echo $name ; ?>"><?php echo $stockName."&nbsp;&nbsp;&nbsp;Rs.".round($price,2) ; ?></option>
                		<?php
					}
				?>
			</select>
			  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
			<input type="button" class="buttonStyle" name="optionsbutton" id="optionsbutton" value="Get Options" onclick= "javascript:getOptions(document.getElementsByName('companynameforoptions')[0].value);" />
	</div>
	 &nbsp;
	  &nbsp;
	<div id ="optionStats"> </div>
	</div>
<p>&nbsp;</p>
<h1 align="left">Company Stats</h1>
<div class="table2" align="center">
<div class="stats"><b>Select a company to analyse</b>
<p>&nbsp;</p>
			<select name="companynameforstats" id="companynameforstats">
		<?php
			$home = file_get_contents('../stocks/stats.txt');
	$stock = explode("~",$home);
	$p = 0;
	

	while($stock[$p]!="")
		{
				
				print("<option value='".$stock[$p]."'>".$stock[$p]." </option>");
				$p = $p+1;
				
		}
		print("</select>");
	
	?>
			
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	 
			<input type="button" class="buttonStyle" name="statsbutton" 
			id="statsbutton" value="Get Info" 
			onclick= "javascript:getStats(document.getElementsByName('companynameforstats')[0].value);" />
		</div>
		 &nbsp;
	  &nbsp;
		<div id = "companyStats"> </div>
		</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div align='center'><h3>For new rules, refer to the Help Section</h3></div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2839740-7");
pageTracker._trackPageview();
} catch(err) {}</script>
