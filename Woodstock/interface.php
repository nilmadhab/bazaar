<font size='-1'>
<div id="heading">Trading stocks</div>
<p>

Go to the Trade by clicking on <strong>trade</strong> tab.
Here you can trade  the stocks.Choose  the name of the company , mention the quantity of stocks and select the nature of the trade(Buy or Short Sell) and place the order by clicking on <strong>place order</strong> button.If you want to place a pending order then fill-in the  <strong>pending order at price</strong>  box with the desired price and then
Place the order by clicking on <strong>place order</strong> button.If the pending order at price is not filled then corresponding order will be considered as immediate order  at which it will be  executed with the current market prices.</p>

<div id="heading">Selling the stocks</div>
<p>
Go to terminal  by clicking on terminal Tab
Here you can see his stock holdings and To sell the stocks fill in the quantity and click on the close button of the stocks of corresponding company. To  sell at particular price then  fill  the <strong>pending price</strong> and sell those stocks.
</p>


<div id="heading">History</div>
<p>
To view the history of your stock trading clickin on the <strong>history</strong> tab</p>
<div id="heading">Some Technical Terms</div>
<p>
<b>Cash in Hand:</b> The amount of virtual cash that you can trade with at any moment of time. <br /><br />
<b>Margin</b> Think of it as a security deposit you make to the broker when you short sell stocks. This amount is subtracted from the 'cash in hand' when you short sell stocks. This is because now that you have deposited the margin(say M), you dont have the amount  M avilable for trading.  '<br /><br />
<b>Cash Balance:</b> The total  'virtual' cash that you have. This is the sum of the 'cash in hand' and the margin money you have deposited for short sell positions. When you have no short sell positions open, your cash balance and 'cash in hand' will be same. However when you short sell stocks some margin (M) is to be deposited .  In this case the cash balance is not affected but your 'cash in hand' decreases by the margin amount M. <br /> <br />

<b>Net Worth:</b> Your worth in the market.Net worth includes the  'cash in hand', margin money deposited (M) and the profit/ loss you are currently making on your transactions ( both buys and short sell positions). Thus, your net worth changes  as the market prices fluctuates as it involves your equivalent cash of the stock positions opened).<br />

Your rank is calculated on the basis of your Net Worth</p><br><br>
</font>
