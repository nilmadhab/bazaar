<?php
	//Code by Tanuj : GUI for futures transaction
	// code updated by PRASHANT for inroducing trigger price and divs
	// code updated finally on 27th of Sept 2010....:)
	include('../session_check.php');
	include('../connection.php');
	include_once('../admin/price.php');
	include('../parameters.php');
	include('friday.php');

	if($tradeallow==2)
	{
		die("Woodstock is closed now.<br/>You cannot trade stock now but can only view your opened stock positions.You can play <a href='http://bazaar.ktj.in/forex'>forex</a> till 12 midnight.");
	}
	if($tradeallow==0)
	{
		die("Both Woodstock and Forex are closed now.<br>You cannot trade stock now but can only view your opened stock positions.You can play Woodstock from 10AM to 4PM only on working days.");
	}
?>
<script type="text/javascript" src="woodstock.js"></script>
<style type="text/css">
<!--
	.table {
	    width: 100%;
	    min-width: 750px;
	}

	.table2 {
	    width: 100%;
	    min-width: 640px;
	}

	.company {

		padding: .81%;
		float: left;
		border: thin none #0000FF;
		width: 18%;
		min-height: 28px;
		margin-left: 17%;
	}

	.select1 {
		padding: .81%;
		float: left;
		border: thin none #0000FF;
		width: 25%;
		min-height: 28px;
		min-width: 260px;
	}

	.company2 {

		padding: .81%;
		float: left;
		border: thin none #0000FF;
		width: 18%;
		min-height: 38px;
		margin-left: 17%;
	}

	.select2 {
		padding: .81%;
		float: left;
		border: thin none #0000FF;
		width: 25%;
		min-height: 44px;
		min-width: 260px;
	}

	.pending {
		padding: .61%;
		float: right;
		border: thin none #0000FF;
		width: 17%;
		min-height: 28px;
		margin-right: .75%;
	}
-->
</style>
<div id="myspan"></div>

<h1 align="left">Trade Futures</h1>
<br/><br/><br/><br/><br/>
<table width="50%" id="brownBorder" style="margin-left:15%;" cellpadding="5" cellspacing="0">
<tr>
<td id="brownBorder"><b>Company</b></td>
<td id="brownBorder">
    	<select name="fstockname" id="fstockname">
	<?php
	
	$home = file_get_contents('fstock.txt');
	$stock = explode("~",$home);
	$p = 0;
	$q = 1;

	while($stock[$p]!="")
		{
				
				print("<option value='".$stock[$p]."'>".$stock[$p]." &nbsp;&nbsp;Rs.".$stock[$q]."</option>");
				$p = $p+2;
				$q = $q+2;
		}
		print("</select>");
		print("</td></tr>");
	?>
             
	<tr>
	<td id="brownBorder" valign="top"><b>Quantity</b></td>
	<td  id="brownBorder">
		<input type="text" name="quantity" id="fquantity">	</td></tr>
		
<tr>
	<td id="brownBorder" valign="top"><b>Long/Short</b></td>
	<td id="brownBorder">
		<select name="bors" id="fbors">
			<option selected value="Long"> Long</option>
			<option value="Short"> Short </option>
		</select>
	</td></tr>
	<tr>
<td  id="brownBorder" valign="top"><b>Expiry</b></td>
	<td id="brownBorder"><select name="expiry" id="fexpiry">
			

<option id ="friday1" value="<?php echo $friday1 ; ?>"  selected><?php echo $friday1 ; ?></option>
			
			
			
		</select></td></tr>
		
		<tr><td id="brownBorder" valign="top"><b>Margin</b></td>
		<td id="brownBorder">
		     <select name="margin" id="fmargin">
			<option value="0.25"> 25% </option>
			<option value="0.30"> 30% </option>
			<option selected value="0.40"> 40% </option>
			<option value="0.50"> 50% </option>
		</select></td></tr>
		<tr>
		<td  id="brownBorder" valign="top"><b>Trigger Price</b><br/>
	<font size="-3">(If left blank will be considered insecure against unlimited losses.)</font></td>
	<td id="brownBorder">
		<input type="text" name="triggerprice" id="triggerprice" value="" />
	</td></tr>
</table>
<div class="pending" style="margin-top:-170px;">
	<input type="button" style="cursor:pointer" class="buttonStyle" 
	name="submit" value="Place Order" onclick="javascript:fget();" /></td></div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>



<h1 align="left">Futures Stats</h1>
<div class="table2" align="center">

<div class="stats"><b>Select a company to view its available Futures:</b>
<p>&nbsp;</p>
			<select name="companynameforfutures" id="companynameforfutures">
                		<?php   
					$query5="SELECT * FROM futures_base ORDER BY stockid ASC";
					$result5=mysql_query($query5);
					
					while($arr5=mysql_fetch_array($result5))
                        		{
                            		$name=$arr5["javaName"];
									$nameChunk=explode('.',$name);
									$nameNew=$nameChunk[0];
                            
                            		$query6="SELECT * FROM stocks_rate WHERE stock='$nameNew'";
                            		$result6=mysql_query($query6);
                            		$arr6=mysql_fetch_array($result6);
                            
                            		$stockName=$arr6["stock"];
                            		$price=$arr6["price"];
			      ?>
						<option value="<?php echo $name ; ?>"><?php echo $stockName."&nbsp;&nbsp;&nbsp;Rs.".round($price,2) ; ?></option>
                		<?php
					}
					
				?>
			</select>
		  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	 
			<input type="button" class="buttonStyle" name="futuresbutton" id="futuresbutton" value="Get Futures" onclick= "javascript:getFutures(document.getElementsByName('companynameforfutures')[0].value);" />
	</div>
	&nbsp;
	  &nbsp;
	<div id = "futureStats"> </div>
</div>

<p>&nbsp;</p>

<h1 align="left">Company Stats</h1>
<div class="table2" align="center">
<div class="stats"><b>Select a company to analyse</b>
<p>&nbsp;</p>
			<select name="companynameforstats" id="companynameforstats">
			<?php
			$home = file_get_contents('../stocks/stats.txt');
	$stock = explode("~",$home);
	$p = 0;
	

	while($stock[$p]!="")
		{
				
				print("<option value='".$stock[$p]."'>".$stock[$p]." </option>");
				$p = $p+1;
				
		}
		print("</select>");
	
	?>
		
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	  &nbsp;
	 
			<input type="button" class="buttonStyle" name="statsbutton" id="statsbutton" value="Get Info" onclick= "javascript:getStats(document.getElementsByName('companynameforstats')[0].value);" />
	</div>
	&nbsp;
	  &nbsp;
	<div id = "companyStats"> </div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<div align='center'><h3>For new rules, refer to the Help Section</h3></div>
<p>&nbsp;</p>	
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-2839740-7");
pageTracker._trackPageview();
} catch(err) {}</script>
