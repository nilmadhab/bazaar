<?php
function startPrice($stock,$week)
	{
		//Code by Tanuj : Calculates startPrice of any futures stock
			
		$stockChunk=explode(".",$stock);
		$stockNew=$stockChunk[0];
		
		$query1="SELECT * FROM stocks_rate WHERE stock='$stockNew'";
		$result1=mysql_query($query1);
		$arr1=mysql_fetch_array($result1);
		$uPrice=$arr1['price'];
		$monday=$arr1['lastMonday'];
		$friday=$arr1['lastFriday'];

		if($friday > $monday)
			$flag=1;
		else
			$flag=-1;

		if($week == 1)
			{
				switch ($uPrice)
						{
							case ($uPrice>=0 && $uPrice<50):
								$randomBCP=0 + (0.0050 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=50 && $uPrice<200):
								$randomBCP=0 + (0.0050 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=200 && $uPrice<500):
								$randomBCP=0 + (0.0050 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=500 && $uPrice<800):
								$randomBCP=0 + (0.0040 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=800 && $uPrice<1200):
								$randomBCP=0 + (0.0030 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=1200 && $uPrice<1600):
								$randomBCP=0 + (0.0025 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=1600):
								$randomBCP=0 + (0.0025 * mt_rand(0, 32767)/32767);
								break;
						}
				}
				
			if($week == 2)
			{
				switch ($uPrice)
						{
							case ($uPrice>=0 && $uPrice<50):
								$randomBCP=0 + (0.0075 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=50 && $uPrice<200):
								$randomBCP=0 + (0.0075 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=200 && $uPrice<500):
								$randomBCP=0 + (0.0075 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=500 && $uPrice<800):
								$randomBCP=0 + (0.0060 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=800 && $uPrice<1200):
								$randomBCP=0 + (0.0045 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=1200 && $uPrice<1600):
								$randomBCP=0 + (0.00375 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=1600):
								$randomBCP=0 + (0.00375 * mt_rand(0, 32767)/32767);
								break;
						}
				}
				
			if($week == 3)
			{
				switch ($uPrice)
						{
							case ($uPrice>=0 && $uPrice<50):
								$randomBCP=0 + (0.01 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=50 && $uPrice<200):
								$randomBCP=0 + (0.01* mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=200 && $uPrice<500):
								$randomBCP=0 + (0.01 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=500 && $uPrice<800):
								$randomBCP=0 + (0.0080 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=800 && $uPrice<1200):
								$randomBCP=0 + (0.0060 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=1200 && $uPrice<1600):
								$randomBCP=0 + (0.0050 * mt_rand(0, 32767)/32767);
								break;
			
							case ($uPrice>=1600):
								$randomBCP=0 + (0.0050 * mt_rand(0, 32767)/32767);
								break;
						}
				}

		//Base Contract Price BCP
		$startPrice = round($uPrice +($flag * $uPrice * $randomBCP),2);

		return($startPrice);
	}

	function valid_pp($triggerprice)
{
	if(ereg("^[0-9\.]+$",$triggerprice))
		return true;
	else
		return false;
}
	
function contractPrice($stock,$week)
	{
		//Code by Tanuj : Calculates contractPrice of any futures stock
	
		$query="SELECT * FROM futures_base WHERE javaName='$stock'";
		$result=mysql_query($query) or die("Could not select stock.");
		$arr=mysql_fetch_array($result);

		if($week == 1)
			{
				$start = $arr['contractPrice1'];
			}
		
		if($week == 2)
			{
				$start = $arr['contractPrice2'];
			}

		if($week == 3)
			{
				$start = $arr['contractPrice3'];
			}

		
		$stockChunk=explode(".",$stock);
		$stockNew=$stockChunk[0];
		
		$query1="SELECT * FROM stocks_rate WHERE stock='$stockNew'";
		$result1=mysql_query($query1);
		$arr1=mysql_fetch_array($result1);
		$uPrice=$arr1['price'];
		$monday=$arr1['lastMonday'];
		$friday=$arr1['lastFriday'];
		$difference=$arr1['diff'];
		
		//Present Percentage Change PPC
		//$ppc = ($uPrice - $friday)/$friday;
		//$contractPrice = (($ppc*(1+$randomCP))+1) * $start ; 

		//$ppc = $difference/$friday;
		// edited from .2 to 0
		$randomCP= 0 + (0.2* mt_rand(0, 32767)/32767);
		echo $randomCP.'<br />';
		echo $start.'<br />';
		echo $difference.'<br />';
		$add= $difference*(0+$randomCP);
		echo $add.'<br />';
		//$contractPrice = ($ppc*(1+$randomCP)) * $start ; 
		$contractPrice = $start + $add;
		echo $contractPrice.'<br />';
		
		
		return($contractPrice);
	}

function roundoff($price)
	{
		// Round offs any price's last decimal place to 0 or 5
	
		$price=round($price,2);
		//echo $price."~";

		$lastDigit=round(($price * 100),0) % 10;

		if($lastDigit < 0)
			$lastDigit = -$lastDigit;

		//echo $lastDigit."*";
		
		if($lastDigit == 0)
			$add = 0.00;
			
		if($lastDigit == 1)
			$add = -0.01;
			
		if ($lastDigit == 2)
			$add = -0.02;
			
		if ($lastDigit == 3)
			$add = 0.02;
								
		if ($lastDigit == 4)
			$add = 0.01;
			
		if ($lastDigit == 5)
			$add = 0.00;
								
		if ($lastDigit == 6)
			$add = -0.01;
			
		if ($lastDigit == 7)
			$add = -0.02;
			
		if ($lastDigit == 8)
			$add = 0.02;
								
		if ($lastDigit == 9)
			$add = 0.01;
			
		if($price > 0)	
			$price = $price + $add;
		else
			{
				//echo "Negative^^".$add."###";
				$price = $price-$add;
			}

		return($price);
	}

	
?>
<script type="text/javascript" src=" http://www.google-analytics.com/urchin.js "></script>
<script type="text/javascript">
_uacct = "UA-2839740-3";
urchinTracker();
</script>