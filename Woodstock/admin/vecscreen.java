/*import package*/
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;
import org.apache.axis.utils.Options;
import javax.xml.rpc.ParameterMode;
import javax.xml.namespace.QName;
import java.net.URL;

public class AxisTest {
	public static void main ( String args[] ) throws Exception {
		/*specifies WSDL file*/
		String wsdlURL = "http://xml.nig.ac.jp/wsdl/GetEntry.wsdl";
		/*specifies name space*/
		String nmspace = "http://www.themindelectric.com/wsdl/GetEntry/";
		/*specifies service name*/
		String srvname = "GetEntry";
		/*specifies function name(method name)*/
		String fncname = "getDDBJEntry";
		/*specifies query( in this case:accession number)*/
		String query   =  "AB000003";
		
		/*specifies service QName and portQName to make Service*/
		QName serviceQN = new QName(nmspace, srvname);
		QName portQN    = new QName(nmspace, srvname);
		/*specifies Service*/
		Service service = new Service(new URL(wsdlURL), serviceQN);
		/*Call SOAP Service*/
		Call call = (Call)service.createCall(portQN, fncname);
		String result = (String)call.invoke(new Object[]{query});
	}
}
